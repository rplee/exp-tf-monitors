include {
  path = find_in_parent_folders()
}

inputs = {
  name    = "test-tg-cpumonitor"
  type    = "metric alert"
  message = "CPU usage alert"
  query   = "avg(last_1m):avg:system.cpu.system{*} by {host} > 60"
}