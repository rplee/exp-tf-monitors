terraform {
  required_providers {
    site24x7 = {
      source = "Bonial-International-GmbH/site24x7"
    }
  }
}

provider "site24x7" {
  oauth2_client_id = var.oauth2_client_id
  oauth2_client_secret = var.oauth2_client_secret
  oauth2_refresh_token = var.oauth2_refresh_token
  retry_min_wait = 1
  retry_max_wait = 30
  max_retries = 4
  api_base_url = "https://www.site24x7.com/api"
  token_url = "https://accounts.zoho.com/oauth/v2/token"
}
