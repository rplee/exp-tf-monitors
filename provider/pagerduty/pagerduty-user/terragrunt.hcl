terraform {
  source = "git::ssh://${get_env("BITBUCKET_USER")}@bitbucket.org/exp-realty/exp-tf-modules.git//provider/pagerduty/resource/pagerduty-user?ref=v3.1.4"
}
