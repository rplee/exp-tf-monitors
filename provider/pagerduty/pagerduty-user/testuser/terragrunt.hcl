include {
  path = find_in_parent_folders()
}

inputs = {
  name  = "Test User"
  email = "pens719@gmail.com"
  role = "admin"
}
