# exp-tf-monitors

Repository containing Terraform templates for all monitoring/alerting providers:

* Datadog
* Site24x7
* PagerDuty
* Status.io

## Quick Start Guide

See the "Reference" section below to learn more about Terraform, Terragrunt, etc.

### Install tfenv, terraform, then terragrunt
    
    brew install tfenv
    tfenv install 0.14.7
    brew install terragrunt

### tfenv usage

    tfenv use 0.14.7
    
Note: now, whenever `terraform` is executed in a directory which contains a `.terraform-version` file, tfenv will automatically download the appropriate version of Terraform.
    
### Setup Bitbucket user shell environment variable
    cd ~/
    # open .zshrc or .bash_profile
    # append to file
    export BITBUCKET_USER="<bitbucket-username-goes-here>"
    
### Plan
    cd <directory-containing-terragrunt.hcl>
    terragrunt plan
        
### Apply
    terragrunt apply

### Configure Terraform plugin caching (for improved runtime performance when running terragrunt apply)
    # change to home directory
    cd ~
    echo -n 'plugin_cache_dir = "$HOME/.terraform.d/plugin-cache"' > .terraformrc
    
### Clean up
    cd <directory-containing-terragrunt.hcl>
    terragrunt destroy

## References

### Terraform
* http://man.hubwiz.com/docset/Terraform.docset/Contents/Resources/Documents/docs/commands/cli-config.html


### Terragrunt
* https://transcend.io/blog/why-we-use-terragrunt
* https://terragrunt.gruntwork.io/docs/getting-started/install/
   
### tfenv
* https://blog.gruntwork.io/how-to-manage-multiple-versions-of-terragrunt-and-terraform-as-a-team-in-your-iac-project-da5b59209f2d
* https://github.com/tfutils/tfenv
* https://github.com/tfutils/tfenv#terraform-version
* https://github.com/tfutils/tfenv#terraform-version-file

### Documentation
- [Workflow overview](https://bitbucket.org/exp-realty/exp-tf-non-prod/src/master/docs/workflow.md)  
- [List of useful commands](https://bitbucket.org/exp-realty/exp-tf-non-prod/src/master/docs/useful-commands.md)

### Modules
- [SAM Pipeline](https://bitbucket.org/exp-realty/exp-tf-non-prod/src/master/docs/modules/sam-pipeline.md)
- [Application Secret](https://bitbucket.org/exp-realty/exp-tf-non-prod/src/master/docs/modules/secret.md)